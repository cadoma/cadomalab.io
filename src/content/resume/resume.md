## Experience

### Sr. Design Technologist – Ameriprise Financial
#### Sep 2021 – Present
* Delivered dozens of accessibility improvements to comply with WCAG 2.1/2.2 AA,  (React, HTML/CSS/JS)
* Technical team lead and mentor to colleagues on a team of 3 engineers
* Led education, adoption and migration to Figma from Sketch
* Improved team agile processes by adopting frequent stand-ups and weekly backlog grooming
* Refactored repository to use CSS preprocessor LESS (eliminating dozens of dependencies)
* Upgraded CI/CD configuration to expedite releases (Jenkins, Docker, Artifactory), allowing for releases at-will rather than a slower, step-by-step release
* Hosted office hours 3 times a week where I supported developers on product teams

### UI Developer – Hamm Interactive / *Ameriprise Financial*
#### Feb 2020 – Sep 2021
* Contributed several changes to design system as external contributor, including adding a new dynamic toggle to the documentation site to demonstrate how components behave with different attributes (e.g. `:disabled`)
* Refactored annual report web application, reducing custom CSS from several hundred lines to less than 100
    * Addressed accessibility issues and ensured appropriate captions and alt text were provided for any visual information
* Conducted accessibility audit on major user-facing application, compiled results and action plan which resulted in a 12 point (17%) improvement on automated scan results

### Full Stack Developer – UnitedHealth Group / Optum
#### Jan 2018 – Jan 2020
* Automated project data collation and organization, reducing manual process from 2+ days to around 10 seconds of effort.
* Developed Domo analytics board to display, summarize, compare data across projects
* Improved Angular-based automated claims processing application
* Migrated CI/CD pipeline to Artifactory

## Certifications

* [Certified Professional in Accessibility Core Competencies](https://www.credly.com/badges/f94adff6-9325-4139-828b-2f2177accc06/public_url)
* [Nielsen Norman Group - UX Certificate (1055208)](https://www.nngroup.com/ux-certification/people/)
* [UserZoom Advanced Certification](http://verify.skilljar.com/c/bksa5siywjdt)

## Education
B.A. Computer Science – Gustavus Adolphus College